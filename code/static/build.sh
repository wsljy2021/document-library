CC=~/toolchains/gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-gcc
OBJDUMP=~/toolchains/gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-objdump
READELF=~/toolchains/gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-readelf

${CC} a.c --static -c a.o -nostdlib
${READELF} a.o -a > a_readelf
${CC} b.c --static -c b.o -nostdlib
${READELF} b.o -a > b_readelf
${CC}  a.o b.o -o ab --static -nostdlib > ab
${READELF} ab -a > ab_readelf
${OBJDUMP} -S -d a.o > a_objdump
${OBJDUMP} -S -d b.o > b_objdump
${OBJDUMP} -S -d ab > ab_objdump