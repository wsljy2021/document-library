CC=~/toolchains/gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-gcc
OBJDUMP=~/toolchains/gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-objdump
READELF=~/toolchains/gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-readelf

# ${CC} a.c -c a.o -nostdlib
# ${READELF} a.o -a > a_readelf

# ${CC} b.c -c b.o  -nostdlib
# ${READELF} b.o -a > b_readelf

# ${CC} libso.c -shared -fpic -nostdlib -o libso.so
# ${READELF} libso.so -a > libso_readelf

# ${CC}  a.o b.o libso.so -o ab -nostdlib -Wl,--verbose  > default_linker
# ${READELF} ab -a > ab_readelf

# ${OBJDUMP} -S -d a.o > a_objdump
# ${OBJDUMP} -S -d b.o > b_objdump
# ${OBJDUMP} -S -d ab > ab_objdump
# ${OBJDUMP} -S -d libso.so > libso_objdump

# CC=gcc
# OBJDUMP=objdump
# READELF=readelf

${CC} a.c -c a.o -nostdlib
${READELF} a.o -a > a_readelf

${CC} b.c -c b.o  -nostdlib
${READELF} b.o -a > b_readelf

${CC} libso2.c -shared -fpic -o libso2.so -nostdlib
${READELF} libso2.so -a > libso2_readelf

${CC} libso.c libso2.so  -shared -fpic -nostdlib  -o libso.so 
${READELF} libso.so -a > libso_readelf

${CC}  a.o b.o libso.so libso2.so -o ab  -Wl,--verbose  > default_linker
${READELF} ab -a > ab_readelf

${OBJDUMP} -S -d a.o > a_objdump
${OBJDUMP} -S -d b.o > b_objdump
${OBJDUMP} -S -d ab > ab_objdump
${OBJDUMP} -S -D libso.so > libso_objdump